package fr.ulille.iut.pizzaland.resources;

import java.net.URI;
import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;

import fr.ulille.iut.pizzaland.dto.IngredientDto;

@Path("/ingredients")
public class IngredientResource {
    private static final Logger LOGGER = Logger.getLogger(IngredientResource.class.getName());

    @Context
    public UriInfo uriInfo;

    public IngredientResource() {
    }

    @GET
    public List<IngredientDto> getAll() {
        LOGGER.info("IngredientResource:getAll");

        return new ArrayList<IngredientDto>();
    }
}
